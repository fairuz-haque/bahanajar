<!DOCTYPE html>
<html lang="id">
    <head>
        <meta charset="UTF-8">
        <title>Digital Bahan Ajar</title>
        <link rel="stylesheet" href="css/style.css"> 
        <link rel="shortcut icon" href="icon/icon-1.png"> 
    </head>

    <body>

        <form name="login-form" class="login-form" action="periksa.php" method="post" enctype="multipart/form-data">

            <div class="header">
                <center><h1>APLIKASI DIGITAL BAHAN AJAR PERKULIAHAN & PRAKTIKUM</h1>
                    <span>D3 MANAJEMEN INFORMATIKA</span>
            </div>

            <div class="content">
                <input name="uname" type="text" class="input username" placeholder="Username" />
                <div class="user-icon"></div>
                <input name="passwd" type="password" class="input password" placeholder="Password" />
                <div class="pass-icon"></div>		
            </div>

            <div class="footer">
                <input type="submit" name="login" id="button" value="Login" class="button" />
            </div>

        </form>

    </body>
</html>

