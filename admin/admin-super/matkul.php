<?php
include "../../connector.php";
if (isset($_POST['tambah'])) {
    mysql_query("INSERT INTO matkul(id_matkul,nm_matkul,bobot_sks,id_smt,quota) Values('$_POST[id_matkul]','$_POST[nm_matkul]','$_POST[bobot_sks]','$_POST[id_smt]','$_POST[quota]')");
    header("location: ./modul.php?halaman=matkul");
}
if (isset($_POST['edit'])) {
    mysql_query("update matkul set nm_matkul = '$_POST[nm_matkul]',bobot_sks = '$_POST[bobot_sks]',id_smt='$_POST[id_smt]',quota = '$_POST[quota]',quota_terpakai = '$_POST[quota_terpakai]' where id_matkul = '$_POST[id]'");
    header("location: ./modul.php?halaman=matkul");
}
?>
<div class="span12" style="margin-bottom: -40px">
    <?php
    error_reporting(0);
    $halaman = 'matkul';
    switch ($_GET['mode']) {
        default:
        ?>
        <h3 style="color: rgb(36, 160, 218);">Data Mata Kuliah</h3>
        <hr style="border: 1px solid rgb(229, 229, 229)">
        <table class="table table-condensed table-bordered dataTable" style="margin-bottom: 10px">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Matakuliah</th>
                    <th>Bobot SKS</th>
                    <th>Semester</th>
                    <th>Quota</th>
                    <th>Quota Terpakai</th>
                    <th>Pengaturan</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                $query = mysql_query("SELECT m.*, s.smt FROM matkul m JOIN semester s ON m.id_smt = s.id_smt order by s.id_smt");
                while ($data = mysql_fetch_array($query)) {
                    echo "<tr>
                    <td>$no</td>
                    <td>$data[nm_matkul]</td> 
                    <td>$data[bobot_sks]</td>                              
                    <td>$data[smt]</td>   
                    <td>$data[quota] KB</td>
                    <td>$data[quota_terpakai] KB</td>
                    <td class='align-center'><a style='color: rgb(36, 160, 218);' href='?halaman=matkul&mode=edit&id=$data[id_matkul]'><i class='icon-arrow-right-2'></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style='color: rgb(36, 160, 218);' href='?halaman=matkul&mode=hapus&id=$data[id_matkul]'><i class='icon-cancel-2'></i></a></td>
                    </tr>";
                    $no++;
                }
                ?>
            </tbody>
        </table>
        <?php
        break;
        case "edit":
        $data = mysql_fetch_array(mysql_query("SELECT m.*, s.id_smt, d.nip, s.smt, d.nama, k.nm_kategori
            FROM matkul m JOIN semester s ON m.id_smt = s.id_smt
            JOIN dosen d ON m.nip = d.nip 
            JOIN kategori k ON d.id_kategori=k.id_kategori where m.id_matkul = '$_GET[id]'"));
            ?>
            <h3 style="color: rgb(36, 160, 218);">Edit Data</h3>
            <hr style="border: 1px solid rgb(229, 229, 229)">
            <form class="form-horizontal" action="?halaman=matkul" method="post" enctype="multipart/form-data" />
            <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
            <fieldset>
                <div class="control-group">
                    <label class="control-label">Kode Mata Kuliah</label>
                    <div class="controls">
                        <input class="input-large" type="text" readonly name="id_matkul" value="<?php echo $data['id_matkul']; ?>"  required>
                    </div>
                </div> 
                <div class="control-group">
                    <label class="control-label">Nama Mata Kuliah</label>
                    <div class="controls">
                        <input class="input-large" type="text" name="nm_matkul" value="<?php echo $data['nm_matkul']; ?>"  required>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Bobot SKS</label>
                    <div class="controls">
                        <input class="input-large" type="text" name="bobot_sks" value="<?php echo $data['bobot_sks']; ?>"  required>
                    </div>
                </div> 
                <div class="control-group">
                    <label class="control-label">Semester</label>
                    <div class="controls">
                        <select name="id_smt"  required>
                            <option value=""></option>
                            <?php
                            $qsmt = mysql_query("select * from semester order by id_smt");
                            while ($dsmt = mysql_fetch_array($qsmt)) {
                                if ($dsmt['id_smt'] == $data['id_smt']) {
                                    echo "<option value='$dsmt[id_smt]' selected>$dsmt[smt]</option>";
                                } else {
                                    echo "<option value='$dsmt[id_smt]'>$dsmt[smt]</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Quota</label>
                    <div class="controls">
                        <input class="input-large" type="text" name="quota" value="<?php echo $data['quota']; ?>"  required>                         
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Quota Terpakai</label>
                    <div class="controls">
                        <input class="input-large" type="text" readonly="readonly" name="quota_terpakai" value="<?php echo $data['quota_terpakai']; ?>"  required>                         
                    </div>
                </div>

                <div class="form-actions">
                    <button type="submit" name="edit" class="btn btn-primary">Simpan</button>
                    <button type="reset" class="btn" onClick="history.back()">Kembali</button>
                </div>
            </fieldset>
        </form>
        <?php
        break;
        case "tambah":
        ?>
        <h3 style="color: rgb(36, 160, 218);">Tambah Data</h3>
        <hr style="border: 1px solid rgb(229, 229, 229)">
        <form class="form-horizontal" action="?halaman=matkul" method="post" enctype="multipart/form-data" />
        <fieldset> 
            <div class="control-group">
                <label class="control-label">Nama Mata Kuliah</label>
                <div class="controls">
                    <input class="input-large" type="text" name="nm_matkul" required>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Bobot SKS</label>
                <div class="controls">
                    <input class="input-large" type="text" name="bobot_sks" required>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Semester</label>
                <div class="controls">
                    <select name="id_smt" required>
                        <option value=""></option>
                        <?php
                        $qsmt = mysql_query("select * from semester order by id_smt");
                        while ($dsmt = mysql_fetch_array($qsmt)) {
                            echo "<option value='$dsmt[id_smt]'>$dsmt[smt]</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Quota</label>
                <div class="controls">
                    <input class="input-large" type="text" name="quota" required>
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" name="tambah" class="btn btn-primary">Simpan</button>
                <button type="reset" class="btn" onClick="history.back()">Kembali</button>
            </div>
        </fieldset>
    </form>
    <?php
    break;
    case "hapus":
        // $img = mysql_fetch_array(mysql_query("select * from pegawai where id_matkul = '$_GET[id]'"));
        //unlink("./data/pegawai/$img[foto]");
    mysql_query("delete from matkul where id_matkul = '$_GET[id]'");
    header("location: ./modul.php?halaman=matkul");
    break;
}
?>
</div>
