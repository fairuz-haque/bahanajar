<?php
session_start();
//periksa apakah user telah login atau memiliki session
if (!isset($_SESSION['uname']) || !isset($_SESSION['passwd'])) {
    ?><script language='javascript'>alert('Anda belum login. Please login dulu');
            document.location = '../index.php'</script><?php
} else {
    ?>
    <!DOCTYPE html>
    <html lang="id">
        <head>
            <meta charset="utf-8" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
            <meta name="viewport" content="width=device-width">
            <meta name="robots" content="noindex, nofollow">
            <title>Digital Bahan Ajar</title>
            <link rel="stylesheet" type="text/css" href="../css/bootmetro.css">
            <link rel="stylesheet" type="text/css" href="../css/semilight.css">
            <link rel="stylesheet" type="text/css" href="../css/icomoon.css">
            <link rel="stylesheet" type="text/css" href="../css/table.css">
            <link rel="stylesheet" type="text/css" href="../css/app.css">
            <link rel="shortcut icon" href="../icon/icon-1.png">
        </head>
        <body data-accent="blue">
            <header id="nav-bar" class="container-fluid">
                <div class="row-fluid">
                    <div class="span9">
                        <div id="header-container">
                            <h4 style="color: rgb(36, 160, 218);"><img src="../icon/icon-1.png" height="75" width="75">&nbsp;APLIKASI BAHAN AJAR PERKULIAHAN & PRAKTIKUM D3 MANAJEMEN INFORMATIKA</h4>

                        </div>
                    </div>
                    <div id="top-info" class="pull-right">
                        <a href="#" class="pull-left">
                            <div class="top-info-block">
                                <h3 style="color: rgb(36, 160, 218); text-align: right"><?php echo ucfirst($_SESSION['uname']); ?></h3>
                            </div>
                            <div class="top-info-block">
                                <b class="icon-user" style="color: rgb(36, 160, 218);"></b>
                            </div>
                        </a>
                    </div>
                </div>
            </header>
            <div class="container-fluid">
                <div class="subnav">
                    <ul class="nav nav-pills">
                        <li><a href="?halaman=beranda">Beranda</a></li>
                        <li><a href="?halaman=admin">Data Admin</a></li>
                        
                        <li><a href="?halaman=mahasiswa">Data Mahasiswa</a></li>
                        <li><a href="?halaman=dosen">Data Dosen</a></li>
                        <li><a href="?halaman=matkul">Data Mata Kuliah</a></li> 
                         <li><a href="?halaman=relasi">Dosen-Matakuliah</a></li>                       
                        <li><a href="?halaman=jenis">Data Jenis</a></li>
                        <li><a href="?halaman=file">Data File</a></li>
                        <li><a href="?halaman=software">Data Software</a></li>
                        <li class="pull-right"><a href="?halaman=keluar">Keluar</a></li>
                        <li><a href="?halaman=system">Pengaturan</a></li>
                    </ul>
                </div>
            </div>
            <div class="container-fluid" style="margin-bottom: -70px">
                <div class="row-fluid" style="margin-top: 30px; margin-bottom: 30px; min-height: 400px">
                    <?php
                    if (isset($_GET["halaman"])) {
                        $halaman = $_GET["halaman"];
                        if ($halaman == "beranda") {
                            require_once("beranda.php");
                        } elseif ($halaman == 'admin') {
                            require_once("admin.php");
                        } elseif ($halaman == 'dosen') {
                            require_once("dosen.php");
						} elseif ($halaman == 'relasi') {
                            require_once("relasi.php");
                        } elseif ($halaman == 'mahasiswa') {
                            require_once("mahasiswa.php");
                        } elseif ($halaman == 'file') {
                            require_once("file.php");
                        } elseif ($halaman == 'matkul') {
                            require_once ("matkul.php");
                        } elseif ($halaman == 'jenis') {
                            require_once ("jenis.php");
                        } elseif ($halaman == 'software') {
                            require_once ("software.php");
                        } elseif ($halaman == 'system') {
                            require_once ("system.php");
                        } elseif ($halaman == "keluar") {
                            session_destroy();
                            header("location: ../");
                        } else {
                            header("location: ./modul.php?halaman=beranda");
                        }
                    } else {
                        header("location: ./modul.php?halaman=beranda");
                    }
                    echo "\n";
                    ?>
                </div>
                <p>
                    Copyright &copy; <?php echo date("Y"); ?> - Aplikasi Digital Bahan Ajar<br>
                    <span style="color: rgb(36, 160, 218);">D3 Manajemen Informatika</span>
                </p>
            </div>
            <script type="text/javascript" src="../js/jquery.js"></script>
            <script type="text/javascript" src="../js/bootmetro.js"></script>
            <script type="text/javascript" src="../js/table.js"></script>
            <script type="text/javascript">
            $(document).ready(function () {
                $(".dataTable").dataTable({
                    "oLanguage": {
                        "sSearch": "Search",
                        "sLengthMenu": "<a class='btn' href='?halaman=<?php echo $halaman; ?>&mode=tambah' style='padding: 1px 10px; margin-top:-10px'>Tambah Data</a> _MENU_",
                        "sZeroRecords": "Data Tidak Ditemukan",
                        "sInfo": "Menampilkan _START_ - _END_ Dari _TOTAL_ Item",
                        "sInfoEmpty": "Menampilkan 0 - 0 Dari 0 Item",
                        "sInfoFiltered": "",
                        "oPaginate": {
                            "sPrevious": "",
                            "sNext": ""
                        }
                    }
                });
            })
            </script>
        </body>
    </html>
    <?php
}
?>