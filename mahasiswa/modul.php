<?php
session_start();
error_reporting(0);
if (isset($_SESSION["HASHMHS"])) {
    require_once("../connector.php");
    ?>
    <html>
        <head>
            <title>Digital Bahan Ajar</title>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
            <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
            <link href="../web/css/style.css" rel="stylesheet" type="text/css" media="all" />

            <link rel="stylesheet" type="text/css" href="../admin/css/table.css">
            <link rel="shortcut icon" href="../admin/icon/icon-1.png">
            <!-- start top_js_button -->
            <script type="text/javascript" src="../web/js/jquery.min.js"></script>
            <script type="text/javascript" src="../web/js/move-top.js"></script>
            <script type="text/javascript" src="../web/js/easing.js"></script>
            <script type="text/javascript">
                jQuery(document).ready(function ($) {
                    $(".scroll").click(function (event) {
                        event.preventDefault();
                        $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1200);
                    });
                });
            </script>
            <!--Menu-->
            <link rel="stylesheet" href="../styles.css">
            <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
            <script src="../script.js"></script>
        </head>
        <body>
        <body>
            <!-- start header -->
            <div class="header_bg">
                <div class="wrap">
                    <div class="header">
                        <div class="logo">
                            <a href="../web/index.html"><img src="../admin/upload/logo1.png" alt=""/> </a>
                        </div>

                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <!-- start header -->
            <div id='cssmenu'>
                <ul>
                    <li><a href="?halaman=beranda"><span>Beranda</span></a></li>
                    <li class='active has-sub'><a href='#'><span>Data File</span></a>
                        <ul>
                            <?php
                            $qsmt = mysql_query("SELECT * from mahasiswa where npm = '$_SESSION[id]'");
                            $dsmt = mysql_fetch_array($qsmt);
                            ?>
                            <li><a href="<? echo"?halaman=silabus&semester='$dsmt[id_smt]'"; ?>"><span>Silabus</span></a></li>
                            <li><a href="<? echo"?halaman=rpkps&semester='$dsmt[id_smt]'"; ?>"><span>RPKPS</span></a></li>

                            <li class='has-sub'><a href='#'><span>Materi</span></a>
                                <ul>
                                    <?php
                                    $qsmt = mysql_query("SELECT * from mahasiswa where npm = '$_SESSION[id]'");
                                    $dsmt = mysql_fetch_array($qsmt);

                                    $qmatkul = mysql_query("SELECT distinct mhs.id_smt, dm.id_matkul, m.nm_matkul, d.id_kategori from dosen_matkul dm join matkul m on dm.id_matkul=m.id_matkul join dosen d on dm.nip=d.nip join mahasiswa mhs on m.id_smt=mhs.id_smt where mhs.id_smt = '$dsmt[id_smt]' and d.id_kategori = '1'");
                                    while ($dmatkul = mysql_fetch_array($qmatkul)) {
                                        echo "<li><a href='?halaman=materi&matkul=$dmatkul[id_matkul]'>$dmatkul[nm_matkul]</a></li>";
                                    }
                                    ?>

                                </ul>
                            <li class='has-sub'><a href='#'><span>Modul</span></a>
                                <ul>
                                    <?php
                                    $qsmt = mysql_query("SELECT * from mahasiswa where npm = '$_SESSION[id]'");
                                    $dsmt = mysql_fetch_array($qsmt);
                                    $qmatkul = mysql_query("SELECT distinct mhs.id_smt, dm.id_matkul, m.nm_matkul, d.id_kategori from dosen_matkul dm join matkul m on dm.id_matkul=m.id_matkul join dosen d on dm.nip=d.nip join mahasiswa mhs on m.id_smt=mhs.id_smt where mhs.id_smt = '$dsmt[id_smt]' and d.id_kategori = '2'");
                                    while ($dmatkul = mysql_fetch_array($qmatkul)) {
                                        echo "<li><a href='?halaman=file&matkul=$dmatkul[id_matkul]'>$dmatkul[nm_matkul]</a></li>";
                                    }
                                    ?>

                                </ul>
                            <li><a href="?halaman=software"><span>Software</span></a></li>
                            <?php
                            $qsmt = mysql_query("SELECT * from mahasiswa where npm = '$_SESSION[id]'");
                            $dsmt = mysql_fetch_array($qsmt);
                            $qmatkul = mysql_query("SELECT distinct mhs.id_smt, m.id_matkul,m.nm_matkul,d.id_kategori from matkul m join dosen d on m.nip=d.nip join mahasiswa mhs on m.id_smt=mhs.id_smt where mhs.id_smt = '$dsmt[id_smt]' and d.id_jenis = '5'");
                            $dmatkul = mysql_fetch_array($qmatkul);
                            ?>

                            <li><a href="<? echo"?halaman=banksoal&semester='$dsmt[id_smt]'"; ?>"><span>Bank Soal</span></a></li>



                        </ul>

                    <li><a href="?halaman=keluar"><span>Logout</span></a></li>
                </ul>

            </div>

            <script type="text/javascript">
                $(".menu,.search").hide();
                $("#menu").click(function () {
                    $(".menu").toggle();
                    $(".search").hide();
                    $("#search").removeClass("active");
                    $("#menu").toggleClass("active");
                });
                $("#search").click(function () {
                    $(".search").toggle();
                    $(".menu").hide();
                    $("#menu").removeClass("active");
                    $("#search").toggleClass("active");
                    $(".text").focus();
                });
            </script>
            <script type="text/javascript" src="../web/js/script.js"></script>
            <div class="clear"></div>

            <div class="clear"></div>
        </div>
    </div>
    </div>
    <!-- start top_bg -->
    <div class="top_bg">
        <div class="wrap">
            <div class="top">
                <h1 style="color: rgb(36, 160, 218); text-align: right; font-size:30px"><?php echo ucfirst($_SESSION['nama']); ?></h1>
                <h2 style="color: rgb(36, 160, 218); text-align: right; font-size:30px"><?php echo ucfirst($_SESSION['id']); ?></h2>
            </div>
        </div>
    </div>
    <!-- start main -->
    <div class="wrap">
        <div class="main">
            <section class="container clearfix">	

                <?php
                if (isset($_GET["halaman"])) {
                    $halaman = $_GET["halaman"];
                    if ($halaman == "beranda") {
                        require_once("beranda.php");
                    } elseif ($halaman == 'data') {
                        require_once("data.php");
                    } elseif ($halaman == 'mahasiswa') {
                        require_once("mahasiswa.php");
                    } elseif ($halaman == 'file') {
                        require_once("file.php");
                    } elseif ($halaman == 'software') {
                        require_once("software.php");
                    } elseif ($halaman == 'silabus') {
                        require_once("silabus.php");
                    } elseif ($halaman == 'materi') {
                        require_once("materi.php");
                    } elseif ($halaman == 'banksoal') {
                        require_once("banksoal.php");
                    } elseif ($halaman == 'rpkps') {
                        require_once("rpkps.php");
                    } elseif ($halaman == "keluar") {
                        session_destroy();
                        header("location: ../");
                    } else {
                        header("location: ./modul.php?halaman=beranda");
                    }
                } else {
                    header("location: ../");
                }
                echo "\n";
                ?>
                <!-- - - - - - - - - - - - - - end Elements - - - - - - - - - - - - - - - -->			

            </section>
        </div>		
    </div>


    <!-- start main_bg -->
    <div class="main_bg">
        <div class="wrap">
            <div class="main content_top">
                <!-- start span_of_3 -->
                <div class="span_of_3">
                    <div class="span1_of_3">
                        <a href="?halaman=beranda"><img src="../admin/upload/icon3.png" alt=""/></a>
                        <div class="span1_of_3_text">
                            <h4><a href="?halaman=beranda">Beranda</a></h4>

                        </div>
                    </div>
                    <div class="span1_of_3">
                        <a href="?halaman=mahasiswa"><img src="../admin/upload/icon02.png" alt=""/></a>
                        <div class="span1_of_3_text">
                            <h4><a href="?halaman=mahasiswa"><?php echo ucfirst($_SESSION['nama']); ?><br><?php echo ucfirst($_SESSION['id']); ?></a></h4>

                        </div>
                    </div>
                    <div class="span1_of_3">
                        <a href="?halaman=keluar"><img src="../admin/upload/icon1.png" alt=""/></a>
                        <div class="span1_of_3_text">
                            <h4><a href="?halaman=keluar">Logout</a></h4>

                        </div>
                    </div>
                    <div class="clear"></div>
                </div></div></div>
        <!--  -->

        <!-- start footer --><!-- start footer -->
        <div class="footer_bg1">
            <div class="wrap">
                <div class="footer1">
                    <!-- scroll_top_btn -->
                    <script type="text/javascript">
                $(document).ready(function () {

                    var defaults = {
                        containerID: 'toTop', // fading element id
                        containerHoverID: 'toTopHover', // fading element hover id
                        scrollSpeed: 1200,
                        easingType: 'linear'
                    };


                    $().UItoTop({easingType: 'easeOutQuart'});

                });
                    </script>
                    <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
                    <!--end scroll_top_btn -->
                    <div class="social-icons">

                    </div>
                    <div class="copy">
                        <p><span>&copy; 2016 Aplikasi Digital Bahan Ajar | D3 MANAJEMEN INFORMATIKA</span></p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>	
        <script>window.jQuery || document.write('<script src="../js/jquery-1.7.1.min.js"><\/script>')</script>
        <!--[if lt IE 9]>
                <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
                <script src="js/ie.js"></script>
        <![endif]-->
        <script src="../js/custom.js"></script>
        <script type="text/javascript">
                function getMatkul(id)
                {
                    if (id == "")
                    {
                        document.getElementById("matkul").innerHTML = "";
                        return;
                    }
                    if (window.XMLHttpRequest)
                    {
                        xmlhttp = new XMLHttpRequest();
                    } else
                    {
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    xmlhttp.onreadystatechange = function ()
                    {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                        {
                            document.getElementById("matkul").innerHTML = xmlhttp.responseText;
                        }
                    }
                    xmlhttp.open("GET", "./get_matkul.php?key=" + id, true);
                    xmlhttp.send();
                }
        </script>
        <script type="text/javascript" src="../admin/js/jquery.js"></script>
        <script type="text/javascript" src="../admin/js/bootmetro.js"></script>
        <script type="text/javascript" src="../admin/js/table.js"></script>
        <script type="text/javascript">
                $(document).ready(function () {
                    $(".dataTable").dataTable({
                        "oLanguage": {
                            "sSearch": "Search",
                            "sLengthMenu": "Data File:&nbsp; _MENU_",
                            "sZeroRecords": "Data Tidak Ditemukan",
                            "sInfo": "Menampilkan _START_ - _END_ Dari _TOTAL_ Item",
                            "sInfoEmpty": "Menampilkan 0 - 0 Dari 0 Item",
                            "sInfoFiltered": "",
                            "oPaginate": {
                                "sPrevious": "",
                                "sNext": ""
                            }
                        }
                    });
                })
        </script>

    </body>
    </html>
    <?php
    mysql_close();
} else {
    header("location: ./");
}
?>