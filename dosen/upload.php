   
<?php
session_start();
include "../connector.php";
if (isset($_POST['upload'])) {
    $uFile = $_FILES['upload_file']['tmp_name'];
    if (!empty($uFile)) {
        $randString = md5(time());
        $fileName = $_FILES["upload_file"]["name"];
        $fileSize = $_FILES["upload_file"]["size"];
        $roundfileSize = round($fileSize / 1024);
        $splitName = explode(".", $fileName);
        $fileExt = end($splitName);
        $dirupload = "../admin/data/";
        $newFileName = strtolower(substr($randString, 0, 15) . '.' . $fileExt);
        $iddosen = $_SESSION['id'];

        $iddosenmatkul = mysql_query("select id_matkul from dosen_matkul where nip = $iddosen");
        $ambilmatkul = mysql_fetch_assoc($iddosenmatkul);
        
        $query_size = "SELECT value from config where nama_id = 'limit_upload'";
        $quota_size = "SELECT * from matkul WHERE id_matkul = $ambilmatkul[id_matkul]";
        
        $get_size = mysql_query($query_size);
        $get_quota = mysql_query($quota_size);

        $limit_size = mysql_fetch_assoc($get_size);
        $quota = mysql_fetch_assoc($get_quota);

        $tambah_quota = $quota['quota_terpakai'] + $roundfileSize;
        if ($tambah_quota < $quota['quota']) {
            if ($roundfileSize < $limit_size['value']) {
                if (move_uploaded_file($uFile, $dirupload . $newFileName)) {
                    $sql = "insert into data (nm_data, nm_file, id_jenis, id_dm, file_size) values('$_POST[judul]','$newFileName','$_POST[jenis]','$_POST[matkul]','$roundfileSize')";
                    if (mysql_query($sql)) {
                        $update_quota = "UPDATE matkul SET quota_terpakai = $tambah_quota WHERE id_matkul = $ambilmatkul[id_matkul]";
                        mysql_query($update_quota);
                        echo "Proses Update Database Berhasil";
                        header("location: ./modul.php?halaman=data");
                    } else {
                        echo "Proses Update Database Gagal";
                        echo mysql_error();
                    }
                } else {
                    echo "Proses Upload Dokumen Gagal";
                }
            } else {
                echo "Besar File melebihi batas";
            }
        } else {
            echo "Quota Matkul sudah melebihi limit yang di tentukan";
        }
    } else {
        echo 'UPLOAD GAGAL';
        header("location: ./modul.php?halaman=upload");
    }
}
?>
<style type="text/css" scoped>

button,
input,
select,
textarea {
    font-size: 100%;
    margin: 0;
    vertical-align: baseline;
    *vertical-align: middle;
}
button,
input {
    line-height: normal; /* 1 */
}

button,
input[type="button"],
input[type="reset"],
input[type="submit"] {
    cursor: pointer; /* 1 */
    -webkit-appearance: button; /* 2 */
    *overflow: visible;  /* 3 */
}

input[type="checkbox"],
input[type="radio"] {
    box-sizing: border-box; /* 1 */
    padding: 0; /* 2 */
    *height: 13px; /* 3 */
    *width: 13px; /* 3 */
}

button::-moz-focus-inner,
input::-moz-focus-inner {
    border: 0;
    padding: 0;
}
.reg_section {
    padding:0;
    margin: 10px 0;

}
.reg_section h3 {
    font-size: 13px;
    margin: 5px 0;
    color:#666;
}
/* Form */
.register {
    position: relative;
    margin-top:0px;
    padding: 20px 20px 40px;
    background: #AFD8D6;
}
.register:before {
    content: '';
    position: absolute;
    top: -8px;
    right: -8px;
    bottom: -8px;
    left: -8px;
    z-index: -1;
    background: #387069;
    border-radius:7px;
    -webkit-border-radius: 7px;
}

.register input[type=text], .register input[type=password] ,.register select,.register textarea {
    width: 278px;
}
.register p.terms {
    float: left;
    line-height: 31px;
}
.register p.terms label {
    font-size: 12px;
    color: #777;
    cursor: pointer;
}
.register p.terms input {
    position: relative;
    bottom: 1px;
    margin-right: 4px;
    vertical-align: middle;
}
.register p.submit {
    text-align: right;
}

.register-help {
    margin: 20px 0;
    font-size: 11px;
    text-align: center;

    color:#FFFFFF;
}
.register-help a {
    color:#FF3679;
    text-shadow:0 1px #1E0E13;
}

:-moz-placeholder {
    color: #c9c9c9 !important;
    font-size: 13px;
}

::-webkit-input-placeholder {
    color: #ccc;
    font-size: 13px;
}

input {
    font-family: Arial, Helvetica, sans-serif;
    font-size: 14px;
}

input[type=text], input[type=password] ,.register select,.register textarea {
    margin: 5px;
    padding: 0 10px;
    height: 34px;
    color: #000;
    background: #fff;
    border-width: 3px;
    border-style: solid;
    border-color: #32434B #4E9CA3 #436970;
    border-radius:3px;
    -webkit-border-radius: 5px;
    outline:3px solid rgba(200, 105, 137, 0.09);
    -moz-outline-radius:7px;
    -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.12);
    -moz-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.12);
    box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.12);
    margin:10px 0;
}
input[type=text]:focus, input[type=password]:focus{
    border-color: #569EA3;
    outline-color: #A9D3E0;
    outline-offset: 0;
}

input[type=submit] {
    float: left;
    padding:0 10px;
    height: 29px;
    font-size: 12px;
    font-weight: bold;
    color: #528598;
    text-shadow:0 1px #3CB5A6; 
    border-width: 1px;
    border-style: solid;
    border-color: #1DC9C9;
    border-radius: 7px 7px 7px 7px;
    outline: none;
    -webkit-box-sizing: content-box;
    -moz-box-sizing: content-box;
    box-sizing: content-box;
    background-color: #C8E3E8;
    background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #AA1E4D), color-stop(100%, #7D0F33));
    background-image: -webkit-linear-gradient(top, #AA1E4D, #7D0F33);
    background-image:-moz-linear-gradient(center top , #AA1E4D, #7D0F33);
    background-image: -ms-linear-gradient(top, #AA1E4D, #7D0F33);
    background-image: -o-linear-gradient(top, #AA1E4D, #7D0F33);
    background-image: linear-gradient(top, #AA1E4D, #7D0F33);
    -webkit-box-shadow:0 1px #CD4170 inset, 0 1px 2px #93284C;
    -moz-box-shadow:0 1px #CD4170 inset, 0 1px 2px #93284C;
    box-shadow:0 1px #CD4170 inset, 0 1px 2px #93284C;
    margin-bottom: 25px;
}
input[type=submit]:active {
    background: #7D0F33;
    -webkit-box-shadow: inset 0 0 3px rgba(0, 0, 0, 0.2);
    -moz-box-shadow: inset 0 0 3px rgba(0, 0, 0, 0.2);
    box-shadow: inset 0 0 3px rgba(0, 0, 0, 0.2);
}

.lt-ie9 input[type=text], .lt-ie9 input[type=password] {
    line-height: 34px;
}
.register select {
    padding:6px 10px;
    width: 300px;
    color: #777777;
}
.register textarea {
    height: 50px;
    padding: 10px;
    color: #000;
}
.custom-file-input::-webkit-file-upload-button {
    visibility: hidden;
}
.custom-file-input::before {
    margin-left: -10px;
    margin-right: -10px;
    content: 'Select some files';
    display: inline-block;
    background: -webkit-linear-gradient(top, #f9f9f9, #e3e3e3);
    border: 1px solid #999;
    border-radius: 3px;
    padding: 5px 8px;
    outline: none;
    white-space: nowrap;
    -webkit-user-select: none;
    cursor: pointer;
    text-shadow: 1px 1px #fff;
    font-weight: 700;
    font-size: 10pt;
    width: 100%;
}
.custom-file-input:hover::before {
    border-color: black;
}
.custom-file-input:active::before {
    background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9);
}
select {
    color: #000;
}
</style>

<section class="register">
    <h4>Upload File</h4>
    <form method="post" action="?halaman=upload" method="post" enctype="multipart/form-data"/>
    <div class="reg_section password">
        <h3>Semester</h3>
        <select name="semester" id="semester" onchange="getMatkul(this.value)">
            <option value=""></option>
            <?php
            $qsmt = mysql_query("select distinct id_smt from vmatkul where nip= '$_SESSION[id]' order by id_smt");
            while ($dsmt = mysql_fetch_array($qsmt)) {
                echo "<option value='$dsmt[id_smt]'>$dsmt[id_smt]</option>";
            }
            ?>
        </select>
        <h3>Matakuliah</h3>
        <select name="matkul" id="matkul" onchange="getJenis(this.value)">
            <option value=""></option>
        </select>
        <h3>Jenis</h3>
        <select name="jenis" id="jenis">
            <option value=""></option>
        </select>
        <div class="reg_section personal_info">
            <h3>Topik Pembahasan</h3>
            <input type="text" name="judul">
        </div>
        <div class="reg_section password">
            <h3>File</h3>
            <input type="file" name="upload_file" class="custom-file-input">
            <h3>*Max Upload 1500KB</h3>
        </div>
    </div> 
    <p class="submit"><input type="submit" name="upload" value="Upload"></p>
</form>
</section>