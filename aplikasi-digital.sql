-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Waktu pembuatan: 10. Maret 2016 jam 14:04
-- Versi Server: 5.5.16
-- Versi PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `aplikasi-digital`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `uname` varchar(35) NOT NULL,
  `passwd` varchar(35) NOT NULL,
  `nama` varchar(35) NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `uname`, `passwd`, `nama`) VALUES
(4, 'superadmin', 'superadmin', 'admin-super'),
(5, 'adminteori', 'adminteori', 'admin-teori'),
(6, 'adminprak', 'adminprak', 'admin-praktek');

-- --------------------------------------------------------

--
-- Struktur dari tabel `beranda`
--

CREATE TABLE IF NOT EXISTS `beranda` (
  `id_beranda` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  PRIMARY KEY (`id_beranda`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `beranda`
--

INSERT INTO `beranda` (`id_beranda`, `judul`, `isi`) VALUES
(2, 'Digital Bahan Ajar Perkuliahan & Praktikum', '<div><span class="wysiwyg-color-black">Aplikasi digital bahan ajar perkuliahan &amp; Praktikum yang cakupannya sebatas Program Studi D3 Manajemen Informatika ini dapat digunakan dalam penyebaran silabus perkuliahan, materi perkuliahan, modul praktikum, software pendukung praktikum, serta bank soal yang akan meningkatkan aktivitas pembelajaran. Aplikasi ini menjadi sarana berbagi informasi bahan ajar perkuliahan.</span></div><div><br></div><div><span class="wysiwyg-color-green"><span class="wysiwyg-color-black">Manfaat yang dapat diperoleh dari penelitian ini yaitu mempermudah dosen sebagai pengajar dalam berbagi segala bahan pembelajaran dan kepentingan yang mendukung dalam perkuliahan, sedangkan manfaat bagi mahasiswa yaitu mempermudah mahasiswa dalam mendapatkan hak nya sebagai pelajar dalam mendapatkan bahan ajar perkuliahan, software pendukung dalam perkuliahan, serta bank soal. Hal tersebut menjadi lebih fleksibelitas dalam waktu, tempat, serta efektivitas pengajaran.</span><br><br><div><br></div></span></div>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `nama` varchar(20) NOT NULL,
  `nama_id` varchar(20) NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `config`
--

INSERT INTO `config` (`nama`, `nama_id`, `value`) VALUES
('Limit Upload', 'limit_upload', 1500);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data`
--

CREATE TABLE IF NOT EXISTS `data` (
  `id_data` int(11) NOT NULL AUTO_INCREMENT,
  `nm_data` varchar(255) NOT NULL,
  `nm_file` varchar(255) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `file_size` int(11) NOT NULL,
  `id_dm` int(11) NOT NULL,
  PRIMARY KEY (`id_data`),
  KEY `id_jenis` (`id_jenis`),
  KEY `id_dm` (`id_dm`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=704 ;

--
-- Dumping data untuk tabel `data`
--

INSERT INTO `data` (`id_data`, `nm_data`, `nm_file`, `id_jenis`, `file_size`, `id_dm`) VALUES
(689, 'Bank Soal UAS - Algoritma 2', 'cc9c27e21f3c6d7.pdf', 5, 361, 56),
(690, 'Bank Soal UAS - APS', 'b7598e90c0c8b5a.pdf', 5, 216, 29),
(691, 'Bank Soal UAS - Desain Grafis', '5c4685578a8c865.pdf', 5, 105, 58),
(692, 'Bank Soal UAS - Logika Informatika', '2730795c73ac15f.pdf', 5, 285, 47),
(693, 'Bank Soal UAS - Oracle', '37f7c10fb674dc5.pdf', 5, 128, 66),
(694, 'Bank Soal UTS - Algoritma 1', 'abd209edceeef61.pdf', 5, 318, 39),
(695, 'Bank Soal UAS - SIB', '6fca49fb966049e.pdf', 5, 358, 13),
(696, 'Bank Soal UAS - PBO', '1e5d2cc5a325d4c.pdf', 5, 196, 21),
(697, 'Bank Soal UAS - SIM', '8559c78fbe233c6.pdf', 5, 157, 22),
(698, 'Bank Soal UAS - SO', '0d0c287219f165f.pdf', 5, 9, 41),
(699, 'Bank Soal UAS - Statistika', 'e24fc20d6dd5927.pdf', 5, 332, 17),
(700, 'Bank Soal UAS - Struktur Data', '39a7ffa29d01504.pdf', 5, 200, 53),
(701, 'Bank Soal UAS - Visual 2', '0b3861e567abbb8.pdf', 5, 279, 60),
(702, 'Bank Soal UAS - Web 2', '46d1e46b445198e.pdf', 5, 322, 51),
(703, 'Bank Soal UAS - Web Desain', 'c6066cf1f28a755.pdf', 5, 313, 62);

-- --------------------------------------------------------

--
-- Struktur dari tabel `dosen`
--

CREATE TABLE IF NOT EXISTS `dosen` (
  `nip` int(11) NOT NULL AUTO_INCREMENT,
  `uname` varchar(35) NOT NULL,
  `passwd` varchar(35) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `jk` enum('Laki-laki','Perempuan') NOT NULL,
  `id_kategori` int(11) NOT NULL,
  PRIMARY KEY (`nip`),
  KEY `id_kategori` (`id_kategori`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=111 ;

--
-- Dumping data untuk tabel `dosen`
--

INSERT INTO `dosen` (`nip`, `uname`, `passwd`, `nama`, `jk`, `id_kategori`) VALUES
(70, 'prihastuti', 'prihastuti', 'Prihastuti Harsani, M. Si.', 'Perempuan', 1),
(71, 'enengtita', 'enengtita', 'Eneng Tita Tosida, M. Si.', 'Perempuan', 1),
(72, 'tjutawaliyah', 'tjutawaliyah', 'Tjut Awaliyah Z, M. Kom.', 'Perempuan', 1),
(73, 'andichairunnas', 'andichairunnas', 'Andi Chairunnas, M. Pd.', 'Laki-laki', 1),
(74, 'boldson', 'boldson', 'Boldson Herdianto, MMSi.', 'Laki-laki', 1),
(75, 'srisetyaningsih', 'srisetyaningsih', 'Dra. Sri Setyaningsih, M. Si.', 'Perempuan', 1),
(76, 'litakarlitasari', 'litakarlitasari', 'Lita Karlitasari, MMSi.', 'Perempuan', 1),
(77, 'diankartika', 'diankartika', 'Dian Kartika Utami, M. Kom.', 'Perempuan', 1),
(78, 'herfina', 'herfina', 'Dr. Herfina, M. Pd.', 'Perempuan', 1),
(79, 'sufiatulmaryana', 'sufiatulmaryana', 'Sufiatul Maryana, M. Kom.', 'Perempuan', 1),
(80, 'ariesmaesya', 'ariesmaesya', 'Aries Maesya, M. Kom.', 'Laki-laki', 1),
(81, 'iyanmulyana', 'iyanmulyana', 'Iyan Mulyana, M. Kom.', 'Laki-laki', 1),
(82, 'iqbal', 'iqbal', 'M. Iqbal Suriansyah, M. Kom.', 'Laki-laki', 1),
(83, 'soewarto', 'soewarto', 'Prof. Dr.-Ing. Soewarto H.', 'Laki-laki', 1),
(84, 'tatangmuhajang', 'tatangmuhajang', 'Tatang Muhajang, M.Ag.', 'Laki-laki', 1),
(85, 'maizar', 'maizar', 'Maizar, Lc., M. Pdi.', 'Laki-laki', 1),
(86, 'jemyarieswanto', 'jemyarieswanto', 'Jemy Arieswanto, M. Kom.', 'Laki-laki', 1),
(87, 'suhermanto', 'suhermanto', 'Suhermanto, SH, MH.', 'Laki-laki', 1),
(88, 'henny', 'henny', 'Henny Suharyati, M.A.', 'Perempuan', 1),
(89, 'ruliachmad', 'ruliachmad', 'Ruli Achmad Fadjri, SS.', 'Laki-laki', 1),
(90, 'prasetyorini', 'prasetyorini', 'Dr. Prasetyorini', 'Perempuan', 1),
(91, 'sasongko', 'sasongko', 'Drs. Sasongko, MM.', 'Laki-laki', 1),
(92, 'dinisuhartini', 'dinisuhartini', 'Dini Suhartini, S. Kom.', 'Perempuan', 1),
(93, 'asepdenih', 'asepdenih', 'Asep Denih, M.Sc', 'Laki-laki', 1),
(94, 'henrihendrawan', 'henrihendrawan', 'Henri Hendrawan, M. Kom.', 'Laki-laki', 1),
(95, 'leonyagus', 'leonyagus', 'Leony Agus, SP.', 'Perempuan', 1),
(96, 'senaramadona', 'senaramadona', 'Sena Ramadona C, S. Kom.', 'Laki-laki', 1),
(97, 'adeirawan', 'adeirawan', 'Ade Irawan', 'Laki-laki', 2),
(98, 'halimah', 'halimah', 'Halimah Tussadiah, M. Kom.', 'Perempuan', 1),
(99, 'ariequrania', 'ariequrania', 'Arie Qurania, M. Kom.', 'Perempuan', 1),
(100, 'halimahtusadiah', 'halimahtusadiah', 'Halimah Tussadiah, M. Kom.', 'Perempuan', 2),
(101, 'karin', 'karin', 'Karin, M. Si.', 'Perempuan', 2),
(102, 'dini', 'dini', 'Dini Suhartini, S. Kom.', 'Perempuan', 2),
(103, 'nurabadi', 'nurabadi', 'Nur Abadi Ramadhan', 'Laki-laki', 2),
(104, 'Christyawan', 'Christyawan', 'Christyawan Ridanto Pitoyo', 'Laki-laki', 2),
(105, 'dian', 'dian', 'Dian Kartika Utami, M. Kom.', 'Perempuan', 2),
(106, 'emakurnia', 'emakurnia', 'Ema Kurnia, S.Kom.', 'Perempuan', 2),
(107, 'agung', 'agung', 'Agung Prajuhana, M.Kom.', 'Laki-laki', 2),
(108, 'jemy', 'jemy', 'Jemy Arieswanto, M. Kom.', 'Laki-laki', 2),
(109, 'victor', 'victor', 'Victor Ilyas Sugara, S.Kom.', 'Laki-laki', 2),
(110, 'rezasetiadi', 'rezasetiadi', 'Reza Setiadi, S.Kom', 'Laki-laki', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `dosen_matkul`
--

CREATE TABLE IF NOT EXISTS `dosen_matkul` (
  `id_dm` int(11) NOT NULL AUTO_INCREMENT,
  `nip` int(11) NOT NULL,
  `id_matkul` int(11) NOT NULL,
  PRIMARY KEY (`id_dm`),
  KEY `nip` (`nip`),
  KEY `id_matkul` (`id_matkul`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=83 ;

--
-- Dumping data untuk tabel `dosen_matkul`
--

INSERT INTO `dosen_matkul` (`id_dm`, `nip`, `id_matkul`) VALUES
(4, 87, 113),
(5, 72, 128),
(6, 99, 128),
(7, 72, 97),
(8, 79, 97),
(9, 88, 98),
(10, 89, 98),
(11, 83, 129),
(12, 80, 129),
(13, 76, 130),
(14, 77, 130),
(15, 75, 99),
(16, 71, 131),
(17, 71, 100),
(18, 90, 101),
(19, 91, 102),
(20, 76, 114),
(21, 79, 114),
(22, 76, 115),
(23, 92, 115),
(24, 75, 103),
(25, 92, 103),
(26, 93, 116),
(27, 94, 116),
(28, 72, 117),
(29, 73, 117),
(30, 83, 132),
(31, 95, 132),
(32, 81, 118),
(33, 72, 133),
(34, 96, 133),
(35, 72, 119),
(36, 98, 119),
(37, 70, 120),
(38, 80, 120),
(39, 70, 104),
(40, 86, 104),
(41, 70, 105),
(42, 71, 121),
(43, 72, 106),
(44, 73, 122),
(45, 74, 122),
(46, 75, 107),
(47, 99, 107),
(48, 76, 123),
(49, 77, 123),
(50, 72, 124),
(51, 98, 124),
(52, 78, 108),
(53, 79, 108),
(54, 72, 125),
(55, 80, 125),
(56, 70, 109),
(57, 81, 110),
(58, 82, 110),
(59, 83, 126),
(60, 80, 126),
(61, 72, 111),
(62, 98, 111),
(63, 84, 112),
(64, 85, 112),
(65, 83, 127),
(66, 86, 127),
(67, 100, 119),
(68, 97, 116),
(69, 101, 100),
(70, 102, 117),
(71, 103, 120),
(72, 104, 114),
(73, 100, 124),
(74, 105, 130),
(75, 102, 103),
(76, 100, 111),
(77, 106, 97),
(78, 107, 118),
(79, 108, 104),
(80, 109, 128),
(81, 110, 133),
(82, 110, 129);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis`
--

CREATE TABLE IF NOT EXISTS `jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nm_jenis` varchar(15) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  PRIMARY KEY (`id_jenis`),
  KEY `id_kategori` (`id_kategori`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nm_jenis`, `id_kategori`) VALUES
(1, 'Silabus', 1),
(2, 'Materi', 1),
(3, 'Modul', 2),
(4, 'Software', 2),
(5, 'Bank Soal', 1),
(6, 'RPKPS', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nm_kategori` varchar(15) NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nm_kategori`) VALUES
(1, 'Teori'),
(2, 'Praktikum');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

CREATE TABLE IF NOT EXISTS `mahasiswa` (
  `npm` varchar(15) NOT NULL,
  `uname` varchar(35) NOT NULL,
  `passwd` varchar(35) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `jk` enum('Laki-laki','Perempuan') NOT NULL,
  `id_smt` int(11) NOT NULL,
  PRIMARY KEY (`npm`),
  KEY `id_smt` (`id_smt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mahasiswa`
--

INSERT INTO `mahasiswa` (`npm`, `uname`, `passwd`, `nama`, `jk`, `id_smt`) VALUES
('068012046', '068012046', '068012046', 'Lucky Ahmad S.', 'Laki-laki', 5),
('068013001', '068013001', '068013001', 'Indah Dwi Sofiani', 'Perempuan', 5),
('068013002', '068013002', '068013002', 'Zenal Abidin', 'Laki-laki', 5),
('068013003', '068013003', '068013003', 'Albertus Sapto P.', 'Laki-laki', 5),
('068013004', '068013004', '068013004', 'Maxi Elvan M.', 'Laki-laki', 5),
('068013005', '068013005', '068013005', 'Fanny Julia', 'Perempuan', 5),
('068013007', '068013007', '068013007', 'Ellang Rafsanjanie', 'Laki-laki', 5),
('068013008', '068013008', '068013008', 'Rudi Denso', 'Laki-laki', 5),
('068013009', '068013009', '068013009', 'Budi Nipon', 'Laki-laki', 5),
('068013010', '068013010', '068013010', 'Ibnu Harits', 'Laki-laki', 5),
('068013011', '068013011', '068013011', 'Deni Firmansyah', 'Laki-laki', 5),
('068013012', '068013012', '068013012', 'Ela Nurhayati', 'Perempuan', 5),
('068013014', '068013014', '068013014', 'Rival Fitriana Turmuzi', 'Laki-laki', 5),
('068013015', '068013015', '068013015', 'Dwi Nurhadiansyah', 'Laki-laki', 5),
('068013016', '068013016', '068013016', 'Faldhy Eka Prayoga', 'Laki-laki', 5),
('068013017', '068013017', '068013017', 'Nur Abadi Ramadhan', 'Laki-laki', 5),
('068013018', '068013018', '068013018', 'Siti Karmila', 'Perempuan', 5),
('068013019', '068013019', '068013019', 'Rizka Desviolina', 'Perempuan', 5),
('068013021', '068013021', '068013021', 'Cloyd Dominik', 'Laki-laki', 5),
('068013022', '068013022', '068013022', 'Yudha Nugraha', 'Laki-laki', 5),
('068013023', '068013023', '068013023', 'Erina Sari', 'Perempuan', 5),
('068013024', '068013024', '068013024', 'Jelina Marselina', 'Perempuan', 5),
('068013026', '068013026', '068013026', 'Mira Agustina', 'Perempuan', 5),
('068013028', '068013028', '068013028', 'Syavina Agustiyanti', 'Perempuan', 5),
('068013029', '068013029', '068013029', 'Cahyo Kartono', 'Laki-laki', 5),
('068013030', '068013030', '068013030', 'Nada Yolanda', 'Perempuan', 5),
('068013031', '068013031', '068013031', 'Fairuz Haque', 'Perempuan', 5),
('068013032', '068013032', '068013032', 'Lisna Herlina', 'Perempuan', 5),
('068013033', '068013033', '068013033', 'Rizki Adnan', 'Laki-laki', 5),
('068013034', '068013034', '068013034', 'Muhammad Arif', 'Laki-laki', 5),
('068013035', '068013035', '068013035', 'Ajeng Ratna Dewi', 'Perempuan', 5),
('068013037', '068013037', '068013037', 'Januar Taufik', 'Laki-laki', 5),
('068013702', '068013702', '068013702', 'Inkha Sartika', 'Perempuan', 5),
('068014001', '068014001', '068014001', 'Affan Hermawan', 'Laki-laki', 4),
('068014002', '068014002', '068014002', 'Asifa Fauziah Asri', 'Perempuan', 4),
('068014003', '068014003', '068014003', 'Maharani Pratiwi P.', 'Perempuan', 4),
('068014004', '068014004', '068014004', 'Saepul Anwar', 'Laki-laki', 4),
('068014005', '068014005', '068014005', 'Safira Ratna M.', 'Perempuan', 4),
('068014006', '068014006', '068014006', 'Agung Wijaya', 'Laki-laki', 4),
('068014008', '068014008', '068014008', 'Sartika Damayanti', 'Perempuan', 4),
('068014009', '068014009', '068014009', 'M. Linald Aqmal', 'Laki-laki', 4),
('068014010', '068014010', '068014010', 'M. Alwi', 'Laki-laki', 4),
('068014011', '068014011', '068014011', 'Galang Prayangka', 'Laki-laki', 3),
('068014012', '068014012', '068014012', 'Aldi Asmara', 'Laki-laki', 3),
('068014013', '068014013', '068014013', 'Fakhri Abar', 'Laki-laki', 3),
('068014014', '068014014', '068014014', 'Heberti Hutabarat', 'Laki-laki', 3),
('068014015', '068014015', '068014015', 'Triana Widyastuti', 'Perempuan', 3),
('068014017', '068014017', '068014017', 'Yandra Adie Rachmawan', 'Laki-laki', 3),
('068014018', '068014018', '068014018', 'Irma Hermalia', 'Perempuan', 3),
('068014019', '068014019', '068014019', 'Dicki Darmawan', 'Laki-laki', 3),
('068014020', '068014020', '068014020', 'Fauzi Gozali', 'Laki-laki', 3),
('068014021', '068014021', '068014021', 'Winitya canrika Utari', 'Perempuan', 3),
('068014022', '068014022', '068014022', 'Dhimas Dwi Nugroho', 'Laki-laki', 3),
('068014023', '068014023', '068014023', 'Sari Cahya Ningdian', 'Perempuan', 3),
('068014024', '068014024', '068014024', 'Viandha Webby P.', 'Laki-laki', 3),
('068014025', '068014025', '068014025', 'Sudrajat Adi Putra', 'Laki-laki', 3),
('068014026', '068014026', '068014026', 'Herlina', 'Perempuan', 3),
('068014701', '068014701', '068014701', 'Inelza Meidyanti', 'Perempuan', 3),
('068015001', '068015001', '068015002', 'Joshua Faisal Reza', 'Laki-laki', 2),
('068015002', '068015002', '068015002', 'Rudi Setiadi Siregar', 'Laki-laki', 2),
('068015003', '068015003', '068015003', 'Nia Mustika', 'Perempuan', 2),
('068015004', '068015004', '068015004', 'Fenia Pratiwi', 'Perempuan', 2),
('068015005', '068015005', '068015005', 'Feraldi Rizky Akbar', 'Laki-laki', 2),
('068015006', '068015006', '068015006', 'Imam Saputra', 'Laki-laki', 2),
('068015007', '068015007', '068015007', 'Iyang Merlin Andrianti', 'Perempuan', 2),
('068015008', '068015008', '068015008', 'Amru Ardhiannizar', 'Laki-laki', 2),
('068015009', '068015009', '068015009', 'Wasdi', 'Laki-laki', 2),
('068015010', '068015010', '068015010', 'Risma Aulia Dewi', 'Perempuan', 2),
('068015012', '068015012', '068015012', 'Mintarsih', 'Perempuan', 1),
('068015013', '068015013', '068015013', 'Tifani Nurbashtian S.', 'Perempuan', 1),
('068015015', '068015015', '068015015', 'Fajar Ramadhan S.', 'Laki-laki', 1),
('068015016', '068015016', '068015016', 'Aman', 'Laki-laki', 1),
('068015017', '068015017', '068015017', 'Faris Farizqi', 'Laki-laki', 1),
('068015018', '068015018', '068015018', 'Falah Abdurahman H.', 'Laki-laki', 1),
('068015019', '068015019', '068015019', 'Acep Rifaul Islam', 'Laki-laki', 1),
('068015020', '068015020', '068015020', 'Afiz Aziz', 'Laki-laki', 1),
('068015021', '068015021', '068015021', 'Gusti Chandar', 'Laki-laki', 1),
('068015022', '068015022', '068015022', 'Dini Mutia Sari', 'Perempuan', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `matkul`
--

CREATE TABLE IF NOT EXISTS `matkul` (
  `id_matkul` int(11) NOT NULL AUTO_INCREMENT,
  `nm_matkul` varchar(35) NOT NULL,
  `bobot_sks` varchar(10) NOT NULL,
  `id_smt` int(11) NOT NULL,
  `quota` int(11) NOT NULL,
  `quota_terpakai` int(11) NOT NULL,
  PRIMARY KEY (`id_matkul`),
  KEY `id_smt` (`id_smt`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=134 ;

--
-- Dumping data untuk tabel `matkul`
--

INSERT INTO `matkul` (`id_matkul`, `nm_matkul`, `bobot_sks`, `id_smt`, `quota`, `quota_terpakai`) VALUES
(97, 'Pengantar Teknologi Informasi', '3(2-1)', 1, 15000, 0),
(98, 'Bahasa Inggris', '2(2-0)', 1, 15000, 0),
(99, 'Matematika Dasar', '2(2-0)', 1, 15000, 0),
(100, 'Statistika', '3(2-1)', 1, 15000, 332),
(101, 'Character Building & Softskill', '3(3-0)', 1, 15000, 0),
(102, 'Bahasa Indonesia', '2(2-0)', 1, 15000, 0),
(103, 'Pengantar Ekonomi & Akuntansi', '3(2-1)', 1, 15000, 0),
(104, 'Algoritma & Pemrograman 1', '3(2-1)', 1, 15000, 0),
(105, 'Sistem Operasi', '3(2-1)', 2, 15000, 9),
(106, 'Basis Data', '3(2-1)', 2, 15000, 0),
(107, 'Logika Informatika', '2(2-0)', 2, 15000, 285),
(108, 'Struktur Data', '3(2-1)', 2, 15000, 200),
(109, 'Algoritma & Pemrograman 2', '3(2-1)', 2, 15000, 361),
(110, 'Aplikasi Desain Grafis', '3(2-1)', 2, 15000, 105),
(111, 'Web Desain', '3(2-1)', 2, 15000, 313),
(112, 'Agama Islam', '3(2-1)', 2, 15000, 0),
(113, 'Pend. Kewarganegaraan', '2(2-0)', 3, 15000, 0),
(114, 'Pemrograman Berorientasi Obyek', '3(2-1)', 3, 15000, 196),
(115, 'Sistem Informasi Manajemen', '3(3-0)', 3, 15000, 157),
(116, 'Jaringan Komputer', '3(2-1)', 3, 15000, 0),
(117, 'Analisa Perancangan Sistem', '3(2-1)', 3, 15000, 216),
(118, 'Teknik Animasi & Multimedia', '3(2-1)', 3, 15000, 0),
(119, 'Pemrograman Web 1', '3(2-1)', 3, 15000, 0),
(120, 'Pemrograman Visual 1', '3(2-1)', 3, 15000, 318),
(121, 'Metode Penelitian', '2(2-0)', 4, 15000, 0),
(122, 'Rekayasa Perangkat Lunak', '3(2-1)', 4, 15000, 0),
(123, 'Otomatisasi Perkantoran', '3(3-0)', 4, 15000, 0),
(124, 'Pemrograman Web 2', '3(2-1)', 4, 15000, 322),
(125, 'Pemrograman Java Mobile', '3(2-1)', 4, 15000, 0),
(126, 'Pemrograman Visual 2', '3(2-1)', 4, 15000, 279),
(127, 'Pemrograman Oracle', '3(2-1)', 4, 15000, 128),
(128, 'E-Technology', '3(2-1)', 5, 15000, 0),
(129, 'Pemrograman Visual 3', '3(2-1)', 5, 15000, 0),
(130, 'Sistem Informasi Bisnis', '3(2-1)', 5, 15000, 358),
(131, 'Manajemen Proyek', '2(2-0)', 5, 15000, 0),
(132, 'Etika Profesi & Kewirausahaan', '2(2-0)', 5, 15000, 0),
(133, 'Pemrograman Android', '3(2-1)', 5, 15000, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `semester`
--

CREATE TABLE IF NOT EXISTS `semester` (
  `id_smt` int(11) NOT NULL AUTO_INCREMENT,
  `smt` varchar(15) NOT NULL,
  PRIMARY KEY (`id_smt`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `semester`
--

INSERT INTO `semester` (`id_smt`, `smt`) VALUES
(1, '1'),
(2, '2'),
(3, '3'),
(4, '4'),
(5, '5'),
(6, '6');

-- --------------------------------------------------------

--
-- Struktur dari tabel `software`
--

CREATE TABLE IF NOT EXISTS `software` (
  `id_software` int(11) NOT NULL AUTO_INCREMENT,
  `nm_software` varchar(35) NOT NULL,
  `link` varchar(255) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  PRIMARY KEY (`id_software`),
  KEY `id_jenis` (`id_jenis`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data untuk tabel `software`
--

INSERT INTO `software` (`id_software`, `nm_software`, `link`, `id_jenis`) VALUES
(3, 'Borland', 'https://drive.google.com/folderview?id=0B1x0IaWMaBHzY0hhc0gzcFVIa0E&usp=sharing', 4),
(4, 'Cisco Packet Tracer', 'https://drive.google.com/folderview?id=0B1x0IaWMaBHzSkVhYm5ZUTd0UEE&usp=sharing', 4),
(5, 'Corel Draw X5', 'https://drive.google.com/folderview?id=0B1x0IaWMaBHzbVAtd1U4d2tIUUk&usp=sharing', 4),
(6, 'Notepad++', 'https://drive.google.com/folderview?id=0B1x0IaWMaBHzakkxZVBpeDFxYTQ&usp=sharing', 4),
(7, 'Photoshop CS5', 'https://drive.google.com/folderview?id=0B1x0IaWMaBHzREJ2bDgyeXNyZG8&usp=sharing', 4),
(8, 'Xampp', 'https://drive.google.com/folderview?id=0B1x0IaWMaBHzOVhxSHRjRjBsbUk&usp=sharing', 4),
(9, 'Dotnetfx35', 'https://drive.google.com/file/d/0B1x0IaWMaBHzX0lJazZaYTdWdmM/view?usp=sharing', 4),
(10, 'Minitab', 'https://drive.google.com/file/d/0B1x0IaWMaBHzQlYxb0pQUGNOM0E/view?usp=sharing', 4),
(11, 'Oracle', 'https://drive.google.com/file/d/0B1x0IaWMaBHzb0ZOSFAteERDOG8/view?usp=sharing', 4),
(12, 'Turbo C++', 'https://drive.google.com/file/d/0B1x0IaWMaBHzZkxrOTBRbHkyZzg/view?usp=sharing', 4);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vmatkul`
--
CREATE TABLE IF NOT EXISTS `vmatkul` (
`id_smt` int(11)
,`nm_matkul` varchar(35)
,`nip` int(11)
,`nama` varchar(35)
,`id_dm` int(11)
,`nm_jenis` varchar(15)
,`id_jenis` int(11)
,`nm_kategori` varchar(15)
);
-- --------------------------------------------------------

--
-- Structure for view `vmatkul`
--
DROP TABLE IF EXISTS `vmatkul`;

CREATE ALGORITHM=UNDEFINED DEFINER=`digitalb`@`localhost` SQL SECURITY DEFINER VIEW `vmatkul` AS select `s`.`id_smt` AS `id_smt`,`m`.`nm_matkul` AS `nm_matkul`,`dsn`.`nip` AS `nip`,`dsn`.`nama` AS `nama`,`dm`.`id_dm` AS `id_dm`,`j`.`nm_jenis` AS `nm_jenis`,`j`.`id_jenis` AS `id_jenis`,`k`.`nm_kategori` AS `nm_kategori` from (((((`dosen_matkul` `dm` join `matkul` `m` on((`dm`.`id_matkul` = `m`.`id_matkul`))) join `dosen` `dsn` on((`dsn`.`nip` = `dm`.`nip`))) join `semester` `s` on((`s`.`id_smt` = `m`.`id_smt`))) join `kategori` `k` on((`dsn`.`id_kategori` = `k`.`id_kategori`))) join `jenis` `j` on((`k`.`id_kategori` = `j`.`id_kategori`)));

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `data`
--
ALTER TABLE `data`
  ADD CONSTRAINT `data_ibfk_1` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `data_ibfk_2` FOREIGN KEY (`id_dm`) REFERENCES `dosen_matkul` (`id_dm`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `dosen`
--
ALTER TABLE `dosen`
  ADD CONSTRAINT `dosen_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id_kategori`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `dosen_matkul`
--
ALTER TABLE `dosen_matkul`
  ADD CONSTRAINT `dosen_matkul_ibfk_1` FOREIGN KEY (`nip`) REFERENCES `dosen` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `dosen_matkul_ibfk_2` FOREIGN KEY (`id_matkul`) REFERENCES `matkul` (`id_matkul`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `jenis`
--
ALTER TABLE `jenis`
  ADD CONSTRAINT `jenis_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id_kategori`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD CONSTRAINT `mahasiswa_ibfk_1` FOREIGN KEY (`id_smt`) REFERENCES `semester` (`id_smt`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `matkul`
--
ALTER TABLE `matkul`
  ADD CONSTRAINT `matkul_ibfk_2` FOREIGN KEY (`id_smt`) REFERENCES `semester` (`id_smt`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `software`
--
ALTER TABLE `software`
  ADD CONSTRAINT `software_ibfk_1` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
