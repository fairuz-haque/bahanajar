<?php
session_start();
error_reporting(0);
if (isset($_POST["login"])) {
    if (isset($_POST["uname"]) and isset($_POST["passwd"])) {
        $uname = $_POST["uname"];
        $passwd = $_POST["passwd"];
        $akses = $_POST["akses"];

        if ($akses == "") {
            session_destroy();
            header("location: ./?login=gagal");
        } elseif ($akses == "dosen") {
            include "./connector.php";
            $login_query = mysql_query("select d.*, k.nm_kategori from dosen d join kategori k on d.id_kategori=k.id_kategori where d.uname='$uname' and d.passwd='$passwd'");
            $login_num_rows = mysql_num_rows($login_query);
            if ($login_num_rows > 0) {
                $login_fetch_array = mysql_fetch_array($login_query);
                $_SESSION["HASHDOSEN"] = md5(time());
                $_SESSION["id"] = $login_fetch_array["nip"];
                $_SESSION["nama"] = $login_fetch_array["nama"];
                $_SESSION["nm_kategori"] = $login_fetch_array["nm_kategori"];
                $_SESSION["uname"] = $login_fetch_array["uname"];
                mysql_close();
                header("location: ./dosen/");
            } else {
                mysql_close();
                session_destroy();
                header("location: ./?login=gagal");
            }
        } elseif ($akses == "mahasiswa") {
            include_once("./connector.php");
            $login_query = mysql_query("select * from mahasiswa where uname='$uname' and passwd='$passwd'");
            $login_num_rows = mysql_num_rows($login_query);
            if ($login_num_rows > 0) {
                $login_fetch_array = mysql_fetch_array($login_query);
                $_SESSION["HASHMHS"] = md5(time());
                $_SESSION["id"] = $login_fetch_array["npm"];
                $_SESSION["nama"] = $login_fetch_array["nama"];
                $_SESSION["uname"] = $login_fetch_array["uname"];
                mysql_close();
                header("location: ./mahasiswa/");
            } else {
                mysql_close();
                session_destroy();
                header("location: ./?login=gagal");
            }
        } else {
            session_destroy();
            header("location: ./?login=gagal");
        }
    } else {
        session_destroy();
        header("location: ./?login=gagal");
    }
} else {
    ?>
    <html>
        <head>
            <meta charset="UTF-8">
            <title>Digital Bahan Ajar</title>
            <link rel="stylesheet" href="css/css login/reset.css">
            <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
            <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/css login/font-awesome.min.css'>
            <link rel="stylesheet" href="css/css login/style.css">   
            <link rel="shortcut icon" href="admin/icon/icon-1.png">
        </head>
        <body>
            <!-- Mixins-->
            <!-- Pen Title-->
            <div class="pen-title">
                <h1>Aplikasi Digital Bahan Ajar</h1><span>D3 MANAJEMEN INFORMATIKA</span>
            </div>
            <div class="container">
                <div class="card"></div>
                <div class="card">
                    <h1 class="title">Login</h1>
                    <form action="./" method="post" enctype="multipart/form-data" />
                    <div class="input-container">
                        <input type="text" id="uname" name="uname" required/>
                        <label for="uname">Username</label>
                        <div class="bar"></div>
                    </div>
                    <div class="input-container">
                        <input type="password" id="passwd" name="passwd" required/>
                        <label for="passwd">Password</label>
                        <div class="bar"></div>
                    </div>
                    <div class="input-container">
                        <select name="akses" style="width:100%">
                            <option value="">--Pilih Hak Akses--</option>
                            <option value="dosen">Dosen</option>
                            <option value="mahasiswa">Mahasiswa</option>
                        </select>
                        <div class="bar"></div>
                    </div>
                    <div class="button-container">
                        <button type="submit" name="login">Login</button>
                    </div>

                    </form>
                    <?php
                    if (isset($_GET["login"])) {
                        if ($_GET["login"] == 'gagal') {
                            echo "<p id='gagal'>Login Gagal</p>";
                        }
                    }
                    ?>
                </div>

            </div>

            <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
            <script src="js/index.js"></script>
        </body>
    </html>
    <?php
}
?>